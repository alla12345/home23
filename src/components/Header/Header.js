import './Header.scss';

// export default function Header({ text }) {
//   const handleClick = () => {
//     console.log(text);
//   };

//   return (
//     <header className='header'>
//       <div>
//         текс
//       </div>
//       <button onClick={handleClick}>{text}</button>
//     </header>
//   );
// }

export default function Header({ text, onClick }) {
  return (
    <header className='header'>
      <div className='header__text'>
        текс
      </div>
      <button className="header__button" onClick={onClick}>{text}</button>
    </header>
  );
}