import './Footer.scss';

export default function Footer({ text }) {
  const handleClick = () => {
    console.log(text);
  };
  
  return (
    <footer className='footer'>
      <button className="footer__button" onClick={handleClick}>{text}</button>
    </footer>
  );
}
