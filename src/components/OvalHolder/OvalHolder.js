import './OvalHolder.scss';

export const OvalHolder = ({ color, withHolder }) => {
  return (
    <div className={`oval-holder oval-holder_${color} ${withHolder ? 'oval-holder_is-opacity' : ''}`}>
      
    </div>
  );
};

export default OvalHolder;
