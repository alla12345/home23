import { Fragment } from 'react';
import './App.scss';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import OvalHolder from '../OvalHolder/OvalHolder';

function App() {
  const arr = [
    { color: 'gray'},
    { color: 'yellow'},
    { color: 'black', isOpasity: true },
    { color: 'yellow'},
    { color: 'yellow'},
    { color: 'orange'},
    { color: 'orange'},
    { color: 'yellow'},
  ];

  const handleClick = (text) => {//подсказали
    console.log(text);
  };


  return (
    <Fragment>
      <Header
        text='кнопка-1'
        onClick={() => handleClick('кнопка-1')}//подсказали        
      />
      <main>
        {arr.map((item, index) => (
          <OvalHolder
            key={index}
            color={item.color}
            withHolder={item.isOpasity} 
          />
        ))}
      </main>
      <Footer
        text='кнопка-1'
      />    
    </Fragment>
  );
}

export default App;
